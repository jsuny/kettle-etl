# kettle-etl

#### 介绍
基于kettle开发的ETL数据迁移功能，具有分页功能，理论可稳定迁移大表。
kettle做数据库数据迁移的时候，如果数据库中的表过大（几十G，甚至更大），为避免kettle 出现OOM，笔者画了一个分页、增量的数据迁移流程。不过存在限制：增量更新需要数据库有最近更新时间字段

个人网上也搜了好久，始终没有找到完美的流程。最后笔者结合网上不完整的，自行画出了这个流程。
#### 个人开发环境
kettle7.0  
mysql8.0
#### 软件架构
该流程表面可能对一些人来说有点复杂：一共8个文件，其中2个Job,6个转换。2个job中1个主job，1个子job。

1、数据迁移.kjb                                    主job  
![Image text](./img/7.png)
2、抽取表子任务.kjb                                子job  
![Image text](./img/4.png)									
3、job结束时间.ktr                                 记录主job结束时间  
![Image text](./img/1.png)
4、创建目标库表结构.ktr                            创建表  
![Image text](./img/5.png)
5、总页数与每页大小设置.ktr                        分页设置  
![Image text](./img/8.png)
6、获取表名称.ktr                                  获取需要迁移的表名字  
![Image text](./img/6.png)
7、表名称变量设置.ktr                              设置一些变量  
![Image text](./img/2.png)
8、表数据抽取.ktr                                  用于同步表数据  
![Image text](./img/3.png)
#### 安装教程

1、直接把8个文件拖到kettle客户端中  
2、demo数据源，demo数据库中有分页配置表、记录主job结束的时间  
![Image text](./img/d1.png)

3、demo2数据源  
![Image text](./img/d2.png)  

4、开发时设置为共享数据源（共享后字体变粗）  
![Image text](./img/数据源.png)

5、注意：配置数据源demo,demo2(名字不要改，因为流程中有段java代码直接根据名获取数据源，具体自行查看流程)  

6、数据表说明：  
ff_table:需要下发的数据表  
pagesize:每页大小  
push:  记录任务结束时间。（下次启动任务会根据上次任务结束的时间，做增量更新）    

![Image text](./img/table.png)



7、注意：trans、job中  配置中有 替换变量、执行每个输入行之类的勾选，请留意

#### 使用说明  
1、返回主job界面，运行  
![Image text](./img/start.png)

2、日志  
![Image text](./img/log.png)

3、结果,由于ff_table中就配置三张表，故只迁移了3张表  
![Image text](./img/result.png)

#### 就这样吧  有些trans可合并，自行优化 

 


 


