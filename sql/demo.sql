/*
Navicat MySQL Data Transfer

Source Server         : my
Source Server Version : 50723
Source Host           : localhost:3306
Source Database       : demo

Target Server Type    : MYSQL
Target Server Version : 50723
File Encoding         : 65001

Date: 2019-12-01 18:13:55
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for ff_table
-- ----------------------------
DROP TABLE IF EXISTS `ff_table`;
CREATE TABLE `ff_table` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `table_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ff_table
-- ----------------------------
INSERT INTO `ff_table` VALUES ('1', 'sys_role_menu');
INSERT INTO `ff_table` VALUES ('2', 'sys_user');
INSERT INTO `ff_table` VALUES ('3', 'sys_user_login_log');

-- ----------------------------
-- Table structure for pagesize
-- ----------------------------
DROP TABLE IF EXISTS `pagesize`;
CREATE TABLE `pagesize` (
  `page_size` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pagesize
-- ----------------------------
INSERT INTO `pagesize` VALUES ('3');

-- ----------------------------
-- Table structure for push
-- ----------------------------
DROP TABLE IF EXISTS `push`;
CREATE TABLE `push` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `push_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of push
-- ----------------------------
INSERT INTO `push` VALUES ('1', '2017-01-01 09:23:01');

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) DEFAULT NULL COMMENT '角色ID',
  `menu_id` bigint(20) DEFAULT NULL COMMENT '菜单ID',
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8 COMMENT='角色与菜单对应关系';

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES ('45', '24', '409', '2018-11-23 09:46:07');
INSERT INTO `sys_role_menu` VALUES ('46', '24', '410', '2018-11-23 09:46:07');
INSERT INTO `sys_role_menu` VALUES ('47', '24', '417', '2018-11-23 09:46:07');
INSERT INTO `sys_role_menu` VALUES ('48', '24', '417', '2018-11-23 09:46:07');
INSERT INTO `sys_role_menu` VALUES ('49', '24', '417', '2018-11-23 09:46:07');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL COMMENT '用户登录名(用于登录的）',
  `password` varchar(100) DEFAULT NULL COMMENT '密码',
  `name` varchar(20) DEFAULT NULL COMMENT '用户真实姓名',
  `sex` tinyint(4) DEFAULT '0' COMMENT '性别 0=男/1=女/2=保密 /3=未知',
  `email` varchar(100) DEFAULT NULL COMMENT '邮箱',
  `mobile` varchar(100) DEFAULT NULL COMMENT '手机号',
  `last_login_time` datetime DEFAULT NULL COMMENT '最后登录时间',
  `last_login_ip` varchar(20) DEFAULT NULL COMMENT '最后登录IP',
  `avatar_url` varchar(255) DEFAULT NULL COMMENT '头像缩略图地址',
  `status` tinyint(4) DEFAULT NULL COMMENT '状态  0：禁用   1：正常',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `dept_id` bigint(20) DEFAULT NULL COMMENT '部门id',
  `work_phone` varchar(20) DEFAULT NULL COMMENT '工作电话、座机',
  `birth_date` date DEFAULT NULL COMMENT '出生日期',
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  KEY `dept_id_index` (`dept_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COMMENT='系统用户';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', 'admin', 'e10adc3949ba59abbe56e057f20f883e', '超级管理员', '0', '2434387555@qq.com', '13647910242', null, '192.168.1.88', 'upload/adminAvatar/201707/1499675749475head.jpg', '1', '2018-01-16 14:47:05', null, null, null, '2018-11-23 09:46:07');
INSERT INTO `sys_user` VALUES ('7', 'sa', 'e10adc3949ba59abbe56e057f20f883e', '张三', '1', 'asd@qq.com', '13456465465', null, '192.168.1.88', 'upload/adminAvatar/201707/1499675749475head.jpg', '1', '2018-01-16 14:47:07', null, null, null, '2018-11-23 09:46:07');
INSERT INTO `sys_user` VALUES ('20', 'zhaoxin', 'e10adc3949ba59abbe56e057f20f883e', '赵信', '2', '12312@qq.com', '3124124124', null, null, null, '2', '2018-03-01 16:33:02', null, '53464567547', '2018-02-15', '2018-11-23 14:50:04');

-- ----------------------------
-- Table structure for sys_user_login_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_login_log`;
CREATE TABLE `sys_user_login_log` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '登录日志ID',
  `login_time` datetime DEFAULT NULL COMMENT '登录时间',
  `login_ip` varchar(20) DEFAULT NULL COMMENT '登录IP',
  `user_id` bigint(20) DEFAULT NULL COMMENT '用户ID',
  `operating_system` varchar(50) DEFAULT NULL COMMENT '操作系统',
  `browser` varchar(50) DEFAULT NULL COMMENT '浏览器',
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='用户登录表';

-- ----------------------------
-- Records of sys_user_login_log
-- ----------------------------
INSERT INTO `sys_user_login_log` VALUES ('1', '2018-11-20 15:41:27', '23423', null, null, null, '2019-12-01 09:46:07');

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL COMMENT '用户ID',
  `role_id` bigint(20) DEFAULT NULL COMMENT '角色ID',
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8 COMMENT='用户与角色对应关系';

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES ('62', '1', '13', '2018-11-23 09:46:07');
INSERT INTO `sys_user_role` VALUES ('72', '8', '13', '2018-11-23 09:46:07');
INSERT INTO `sys_user_role` VALUES ('73', '7', '24', '2018-11-23 09:46:07');
INSERT INTO `sys_user_role` VALUES ('75', '10', '24', '2018-11-23 09:46:07');
INSERT INTO `sys_user_role` VALUES ('80', '11', '25', '2018-11-23 09:46:07');
